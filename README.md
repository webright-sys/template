# コーディング規則 【2021.03.08 現在】 #

### 開発環境について ###
- node v12.14.1
- gulp v4.0.2
- npm install
- npx gulp で走ります

### 仕様追加・ファイル修正について ###
- masterから feature/〇〇 でブランチ切って masterへプルリク送ってください

### サポートブラウザ ###

1. PC
IE11,Microsoft Edge 最新版,Chrome 最新版,Firefox 最新版  
AppleMacOSX Safari 最新版  

2. スマホ：
Android 最新版,  
iOS 最新版  

3. タブレットに関しては、各機種での動作確認は必要ありませんが、各ディスプレイの大きさで、綺麗にみえるようにする。

### ディレクトリ構成について ###
```
root/
  ├ assets/
  │  └ css/
  │  └ js/
  │  └ images/
  │      └ common/
  │      └ top/
  └ index.html
```

### レスポンシブ・リキッドコーディング ###

1. ブレイクポイントは2つ（タブレット、スマホ対応）  
  ・1040px以下でタブレット用レイアウト  
  ヘッダーを固定。ロゴ、電話番号、ハンバーガーメニューが常に表示されるように  
  
  ・767px以下でスマホ用レイアウト  
  ヘッダーを固定。ロゴ、電話アイコン、ハンバーガーメニューが常に表示されるように  
  参考：https://www.nawano-sangyou.jp/  

2. 横スクロールが出ないよう調整  
  ・極力、1920px 以上の場合でも、フルワイドになる仕様にする  
  ・広がりすぎると微妙な場合は max-width:1920pxでOKです  

3. 上記2箇所でのレスポンシブが難しい場合は適宜メディアクエリを切る  

### 外部ファイルの読み込み ###
```
  <!--NG -->
  <script src="http://www.google.com/js/"></script>

  <!--OK -->
  <script src="//www.google.com/js/"></script>
  <script src="https://www.google.com/js/"></script>
```

### webfontについて ###
- google fontのみ使用する


### css命名規則について ###
1. 基本はBEMで行う  
  .ページ名__セクション-コンポーネント  
    ex) .top__about-item  

2. idは基本使わない (js操作、ページ内リンク以外)  
  ・js操作の操作のばいは下記のようにする  
  #js-topkeyvisual-slider  


### スタイリングについて ###
- seo観点からh2タグからh3タグへ変更などという場合のときのために、なるべくタグへ直接スタイルをあてない。

```
<div class="top__intro">
  <h3>HOGEHOGE <span>hogehoge</span></h3>
</div>

h3 {
font-size:20px;
}

h3 span {
font-size:14px;
}
```
ではなく、下記のようにする

```
<div class="top__intro">
    <h3 class="top__intro-title">
      HOGEHOGE
      <span class="top__intro-subtitle">hogehoge</span>
  </h3>
</div>

.top__intro-title {
font-size:20px;
}

.top__intro-subtitle {
font-size:14px;
}
```

### jqueryについて ###
1. 一番頭で必ず「"use strict";」を宣言する

2. jqueryライブラリを使用するときはなるべくcdnを使用する

3. カスタマイズを行うときはcdnを読み込んだ上でオーバーライドを行う
  ex) 【2021.03.03 現在】

```
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js" integrity="sha512-p55Bpm5gf7tvTsmkwyszUe4oVMwxJMoff7Jq3J/oHaBk+tNQvDKNz9/gLxn9vyCjgd6SAoqLnL13fnuZzCYAUA==" crossorigin="anonymous"></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css" integrity="sha512-UBY4KATrDAEKgEEU+RAfY4yWrK0ah43NGwNr5o/xmncxsT+rv8jp7MI3a00I0Ep3NbAx05U5kw3DvyNCoz+Wcw==" crossorigin="anonymous" />
```

### スマホのハンバーガーメニューについて ###
1. 原則、pcと同一ソースで切り替える  
  ベース)  
  https://tympanus.net/Development/FullscreenOverlayStyles/  


2. クラスの付替
下記などで行う。  
motion1.jsに関しては下記と同じことを無駄なコード使って行っているのでいらないかも？
```
  var $body = $("body");
  $("#js-navTrigger").click(function () {
    $body.toggleClass("navopen");
  });
```


### 画像の命名について ###
- 省略なしのアンダースコア表記にする  
  ex) top/intro_img01.jpg


### 画像のalt ###
- 必ず設定する
  ex) <img src="./assets/images/top/about_img01.jpg" alt="当社の強み">


### ファビコン ###
- ロゴデータを使用し、ファビコンの設置をする  
  【参考】http://www.02320.net/apple-touch-icon_for_android/  
  ※ロゴ、ローマ字の頭文字でもＯＫです。  

### 電話番号 ###
  <a href="tel:0312345678">は使用しない、.tel_linkを付与する  
  ex)  
  <span class="tel_link">03-1234-5678</span>  

  電話番号が画像の場合はaltに電話番号のみ記載してください。  
  <span class="tel_link"><img src="…" alt="03-1234-5678"></span>  


### margin ###
- 原則、下向きで設定していく

### ホバー時の効果 ###
1. GナビにOn / Off の表現をつける
2. 「詳しく見る」などのボタンは色を反転するなどの効果を付ける


### 社名ロゴ ###
- トップページへのリンクを設定