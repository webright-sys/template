// Common
const $window = $(window).width();
$(function () {
  $("#js-pagetop").click(function () {
    // pagetop
    $("html,body").animate({ scrollTop: 0 }, "300");
  });

  $(".js-smooth").click(function () {
    var adjust = 0;
    var speed = 400;
    var href = $(this).attr("href");
    var target = $(href == "#" || href == "" ? "html" : href);
    var position = target.offset().top + adjust;
    $("body,html").animate({ scrollTop: position }, speed, "swing");
    return false;
  });
});

$(function () {
  // nav
  var $body = $("body");
  $("#js-navTrigger").click(function () {
    $body.toggleClass("navopen");
  });
});

$(function () {
  // header
  const $header = $(".header");
  const headerHeight = $header.height();
  $(window).on("scroll", function () {
    if ($(this).scrollTop() < headerHeight) {
      $header.removeClass("is-scrolled");
    } else {
      $header.addClass("is-scrolled");
    }
  });
});

$(function () {
  // Top Page
  if ($("#js-topkeyvisual-slider").length) {
    $("#js-topkeyvisual-slider").bxSlider({
      auto: true,
      mode: "fade",
      infiniteLoop: true,
      controls: false,
      pager: false,
      minSlides: 1,
      maxSlides: 1,
      moveSlides: 1,
    });
  }

  if ($("#js-topworks-slider").length) {
  $("#js-topworks-slider").bxSlider({
    ticker: true,
    speed: 40000,
    mode: "horizontal",
    infiniteLoop: true,
    controls: false,
    pager: false,
    minSlides: 1,
    maxSlides: 4,
    moveSlides: 1,
    slideWidth: 480,
    slideMargin: 0,
  });
}
});