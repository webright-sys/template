//gulpとgulpのパッケージを読み込み
const { src, dest, watch, lastRun, parallel, series } = require("gulp");
const sass = require("gulp-sass"); // Sassを使う
const glob = require("gulp-sass-glob"); // sassのimportを楽にする
const postcss = require("gulp-postcss"); // Autoprefixと一緒に使うもの
const autoprefixer = require("autoprefixer"); // Autoprefix
const plumber = require("gulp-plumber"); // エラーでも強制終了させない
const notify = require("gulp-notify"); // エラーのときはデスクトップに通知
const htmlmin = require("gulp-htmlmin"); // htmlの圧縮・整理
const del = require("del"); // ファイル、ディレクトリの削除
const ejs = require("gulp-ejs"); // htmlのパーツ化
const concat = require("gulp-concat"); // ファイルの結合
const order = require("gulp-order"); // 指定した順番で並べる
const rename = require("gulp-rename"); // ejsの拡張子を変更
const cleanCSS = require("gulp-clean-css"); // cssの圧縮
const pngquant = require("imagemin-pngquant"); // 画像圧縮の定義
const imagemin = require("gulp-imagemin"); // 画像圧縮
// const mozjpeg = require("imagemin-mozjpeg"); // 画像圧縮 win用
const jpegtran = require("imagemin-jpegtran"); // 画像圧縮 mac用
const uglify = require("gulp-uglify"); // script圧縮
const browserSync = require("browser-sync"); // ブザウザ読み込み・反映
const replace = require("gulp-replace"); // 余計なテキストを削除
const fs = require("fs");
const data = require("gulp-data");
const babel = require("gulp-babel");
const webpackStream = require("webpack-stream");
const webpack = require("webpack");
const webpackConfig = require("./webpack.config");

//開発と本番で処理を分ける
//今回はhtmlの処理のところで使っています
const mode = require("gulp-mode")({
  modes: ["production", "development"],
  default: "development",
  verbose: false,
});

//読み込むパスと出力するパスを指定
const srcPath = {
  html: {
    src: ["./src/ejs/**/*.ejs", "!" + "./src/ejs/**/_*.ejs"],
    dist: "./dist",
  },
  styles: {
    src: "./src/scss/**/*.scss",
    dist: "./dist/assets/css/",
    map: "./dist/assets/css/map",
  },
  scripts: {
    src: "./src/js/**/*.js",
    dist: "./dist/assets/js/",
    map: "./dist/js/map",
  },
  images: {
    src: "./src/images/**/*.{jpg,jpeg,png,gif,svg}",
    dist: "./dist/assets/images/",
  },
};

//htmlの処理自動化
const htmlFunc = () => {
  var jsonData = JSON.parse(fs.readFileSync("src/ejs/config.json"));
  return src(srcPath.html.src)
    .pipe(
      plumber({ errorHandler: notify.onError("Error: <%= error.message %>") })
    )
    .pipe(
      data(function (file) {
        return { filename: file.path };
      })
    )
    .pipe(ejs({ jsonData: jsonData }, {}, { ext: ".html" })) //ejsを纏める
    .pipe(rename({ extname: ".html" })) //拡張子をhtmlに

    .pipe(
      mode.production(
        //本番環境のみ
        htmlmin({
          //htmlの圧縮
          collapseWhitespace: true, // 余白を除去する
          preserveLineBreaks: true, //改行を詰める
          minifyJS: true, // jsの圧縮
          removeComments: true, // HTMLコメントを除去する
        })
      )
    )
    .pipe(replace(/[\s\S]*?(<!DOCTYPE)/, "$1"))
    .pipe(dest(srcPath.html.dist))
    .pipe(browserSync.reload({ stream: true }));
};

//Sassの処理自動化（開発用）
const stylesFunc = () => {
  return src(srcPath.styles.src, { sourcemaps: true })
    .pipe(
      plumber({ errorHandler: notify.onError("Error: <%= error.message %>") })
    )
    .pipe(glob())
    .pipe(sass({ outputStyle: "expanded" }).on("error", sass.logError))
        // nested
        // expanded
        // compact
        // compressed
    .pipe(
      postcss([
        autoprefixer({
          // IEは11以上、Androidは4、ios safariは8以上
          // その他は最新2バージョンで必要なベンダープレフィックスを付与する
          //指定の内容はpackage.jsonに記入している
          cascade: false,
          grid: true,
        }),
      ])
    )
    .pipe(dest(srcPath.styles.dist, { sourcemaps: "./map" }))
    .pipe(browserSync.reload({ stream: true }));
};

//Sassの処理自動化（本番用）
const stylesCompress = () => {
  return src(srcPath.styles.src)
    .pipe(
      plumber({ errorHandler: notify.onError("Error: <%= error.message %>") })
    )
    .pipe(glob())
    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(
      postcss([
        autoprefixer({
          //上の指定と同じ
          cascade: false,
          grid: true,
        }),
      ])
    )
    .pipe(cleanCSS())
    .pipe(dest(srcPath.styles.dist))
    .pipe(browserSync.reload({ stream: true }));
};

// babel トランスパイル
const babelFunc = () => {
  return src(srcPath.scripts.src)
    .pipe(
      babel({
        presets: ["@babel/preset-env"],
      })
    )
    // .pipe(uglify())
    .pipe(dest(srcPath.scripts.dist));
};

// const jsFunc = () => {
//   return src(srcPath.scripts.src)
//     .pipe(webpackStream(webpackConfig, webpack))
//     .pipe(
//       babel({
//         presets: ["@babel/preset-env"],
//       })
//     )
//     .pipe(uglify())
//     .pipe(dest(srcPath.scripts.dist));
// };

// function jsFunc() {
//   return plumber({
//     errorHandler: notify.onError('<%= error.message %>'),
//   })
//   .pipe(webpackStream(webpackConfig, webpack))
//   .pipe(
//     babel({
//       presets: ["@babel/preset-env"],
//     })
//   )
//   .pipe(uglify({}))
//   .pipe(dest(srcPath.scripts.dist));
// }


//画像圧縮の定義
const imagesBase = [
  pngquant({
    quality: [0.7, 0.85],
  }),
  jpegtran({
    quality: 85,
  }),
  // imagemin.jpegtran(),
  // imagemin.jpegtran({
  //   quality: 85,
  //   progressive: true
  // }),
  imagemin.gifsicle(),
  imagemin.optipng(),
  imagemin.svgo({
    removeViewBox: false,
  }),
];

//画像の処理自動化
const imagesFunc = () => {
  return src(srcPath.images.src, { since: lastRun(imagesFunc) })
    .pipe(plumber({ errorHandler: notify.onError("<%= error.message %>") }))
    .pipe(imagemin(imagesBase))
    .pipe(dest(srcPath.images.dist));
};

// マップファイル除去
const cleanMap = () => {
  return del([srcPath.styles.map, srcPath.scripts.map]);
};

// dist全ファイル除去
const cleanAll = () => {
  return del([srcPath.html.dist]);
};

// ブラウザの読み込み処理
const browserSyncFunc = (done) => {
  browserSync({
    server: {
      baseDir: "./dist/",
      index: "index.html",
    },
    port: 4000,
    reloadOnRestart: true,
  });
  done();
};

const browserReload = () => {
  browserSync.reload();
};

// ファイルに変更があったら反映
const watchFiles = () => {
  watch(srcPath.html.src, htmlFunc);
  watch(srcPath.styles.src, stylesFunc);
  watch(srcPath.scripts.src, babelFunc);
  watch(srcPath.images.src, imagesFunc);
};

exports.default = parallel(watchFiles, browserSyncFunc); //gulpの初期処理
exports.build = parallel(htmlFunc, stylesFunc, babelFunc, imagesFunc);

exports.sasscompress = stylesCompress;
exports.cleanmap = cleanMap;
exports.cleanall = cleanAll;
// exports.babel = babelFunc;
exports.images = imagesFunc;
// exports.js = jsFunc;

// 実行 : npx gulp
// 全体ファイル更新 : npx gulp build
// css圧縮 : npx gulp sasscompress
// sourcemap削除 : npx gulp cleanmap
// dist削除 : npx gulp cleanall
// 画像 : npx gulp cleanall
